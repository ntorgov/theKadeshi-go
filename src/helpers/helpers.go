// Package with some useful functions
package helpers

// Функция поиска по массиву. Аналог "is in array"
func StringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}
