package main

import (
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"regexp"
	"strings"
	"time"

	"helpers"
)

// Версия приложения
const AppVersion = "0.0.1"

// Список файлов для сканирования
var fileList []SFileListElement

// Список зараженных файлов
var anamnesisList []SAnamnesisList

// Суммарное количество байт в файлах
var totalBytes int64

// Структура списка файлов
type SFileListElement struct {
	Path string
	Size int64
}

// Структура базы сигнатур
type SSignatures struct {
	H []SSignatureElement `json:"h"` // Сигнатуры для хешей
	R []SSignatureElement `json:"r"` // Сигнатуры для регулярных выражений
}

// Структура сигнатуры
type SSignatureElement struct {
	Expression string `json:"expression"`
	Action     string `json:"action"`
	Title      string `json:"title"`
	Format     string `json:"format"`
	ID         int    `json:"id"`
}

// Структура анамнеза. Прототип
type SAnamnesisList struct {
	Path       string      // Путь к файлу
	Title      string      // Название
	Action     string      // Действие
	CureAction SCureAction // Параметры лечения
}

// Структура параметров лечения
type SCureAction struct {
	Start int // Начало текстового блока
	End   int // Конец текстового блока
}

// Константы цветов для консоли
// @todo:ntorgov их можно убрать
const (
	CRed     = "\x1b[31m"
	CGreen   = "\x1b[32m"
	CYellow  = "\x1b[33m"
	CDefault = "\x1b[39m"
	CLGray   = "\x1b[90m"
	CLRed    = "\x1b[91m"
	CLGreen  = "\x1b[92m"
	CLYellow = "\x1b[93m"
)

// Массив расширений
var validExtensions = []string{".php", ".js", ".htm", ".html"}

// База сигнатур
var signaturesDatabase SSignatures

func main() {

	// arguments := flag.Arg(0)
	fmt.Printf("theKadeshi scanner v%s is ready\n", AppVersion)
	flag.Parse()
	root := flag.Arg(0)
	root = "."
	fmt.Printf("Building file list...")
	err := filepath.Walk(root, GetFileList)

	if err != nil {
		fmt.Printf("filepath.Walk() returned %v\n", err)
		//return 1
	}
	// echo('Files to scan: ' . $totalFiles . PHP_EOL);
	fmt.Printf("\r"+CDefault+"Files to scan: "+CGreen+"%v", len(fileList))
	fmt.Printf(CDefault+" %v bytes\n", totalBytes)
	// Получаем сигнатуры
	GetRemoteSignatures()

	fmt.Printf(CDefault+"Loaded "+CGreen+"%+v"+CDefault+" regular and "+CGreen+"%+v"+CDefault+" hash signature(s)\n", len(signaturesDatabase.R), len(signaturesDatabase.H))
	//return 0

	//fmt.Println(signaturesDatabase.R[3].Expression)

	// Инициализация списка
	// anamnesisList := []SAnamnesisList{}
	scannerStartTime := time.Now().UnixNano()

	Scanner()

	// if len(anamnesisList) > 0 {
	// 	fmt.Println("Total", len(anamnesisList), anamnesisList)
	// }
	fmt.Println("\r\n", time.Now().UnixNano()-scannerStartTime)
}

// Получаем список всех файлов в каталоге
func GetFileList(filePath string, f os.FileInfo, err error) error {
	fi, err := os.Stat(filePath)
	if err != nil {
		return err
	}

	switch mode := fi.Mode(); {

	// Проверка является ли путь файлом
	case mode.IsRegular():

		// Тут мы проверяем совпадает ли расширение со массивом
		if helpers.StringInSlice(path.Ext(filePath), validExtensions) {
			totalBytes = totalBytes + fi.Size()
			fileList = append(fileList, SFileListElement{filePath, int64(fi.Size())})
		}
	}

	return nil
}

// Функция получения удаленных сигнатур
func GetRemoteSignatures() {

	// @todo:ntorgov Путь надо будет перенести в отдельную константу
	response, err := http.PostForm("http://thekadeshi.com/api/getSignatures", url.Values{"notoken": {"1"}})
	if err != nil {
		log.Fatalln("Error reading remote signatures", err)
	}
	defer response.Body.Close()
	body, err := ioutil.ReadAll(response.Body)

	signatureResponse := string(body)

	err = json.Unmarshal([]byte(signatureResponse), &signaturesDatabase)
	if err != nil {
		log.Fatalln("Signatures error", err)
	}

	// Преобразование выражений из PCRE в Golang
	RefactorRegex := regexp.MustCompile(`^[~/'](.*)[~/']([isUuxa]{1,6})$`)
	for index, signature := range signaturesDatabase.R {
		matches := RefactorRegex.FindStringSubmatch(signature.Expression)
		if len(matches) > 0 {
			signaturesDatabase.R[index].Expression = "(?" + strings.Replace(matches[2], "u", "", -1) + ")" + matches[1]
		}
	}
}

// Функция сканирования
func Scanner() {

	// Время начала запуска
	var startTimer int64 = time.Now().UnixNano()

	// Расчетная скорость сканнера
	var scannerSpeed float64 = 0

	// Всего просканировано байт
	var totalScannedBytes int64 = 0

	// Флаг, необходимо ли продолжать сканирование
	var needToContinue bool = true

	var totalFilesCount int = len(fileList)

	// Перебираем все файлы из списка
	for index, element := range fileList {

		totalScannedBytes = totalScannedBytes + element.Size

		fmt.Printf("\r[%.2fkB/s, %v%%] %s", scannerSpeed, float64(100*int64(index+1)/int64(totalFilesCount)), element.Path)

		content, err := ioutil.ReadFile(element.Path)
		if err != nil {
			fmt.Println("Error reading file", err)
		}

		fileHash := sha256.New()
		fileHash.Write(content)
		// if _, err := io.Copy(fileHash, content); err != nil {
		// 	log.Fatal(err)
		// }
		fileHashCode := hex.EncodeToString(fileHash.Sum(nil))
		// fmt.Println(fileHashCode)

		// Перебираем все сигнатуры на hash
	HashLoop:
		for _, signature := range signaturesDatabase.H {
			// fmt.Println(signature.Expression)
			if signature.Expression == fileHashCode {
				anamnesisList = append(anamnesisList, SAnamnesisList{
					Path:       element.Path,
					Title:      signature.Title,
					Action:     signature.Action,
					CureAction: SCureAction{0, 0},
				})
				needToContinue = false
				break HashLoop
			}
		}

		// Если необходимо продолжать сканирование
		// Процесс поиска по регулярным выражениям не быстр, поэтому его по возможности следует избегать
		if needToContinue == true {

			// Перебираем все сигнатуры на регулярных выражениях
		SignatureLoop:
			for _, signature := range signaturesDatabase.R {
				// fmt.Println(signature.Action)
				currentRegexp, err := regexp.Compile(string(signature.Expression))

				if err == nil {

					match := currentRegexp.Match(content)

					if match {
						//fmt.Printf("\r")
						fmt.Printf(CDefault+"\rFound %s in %s [%s]\r\n"+CDefault, CLYellow+signature.Title+CDefault, CYellow+element.Path+CDefault, CGreen+signature.Action+CDefault)
						anamnesisList = append(anamnesisList, SAnamnesisList{
							Path:       element.Path,
							Title:      signature.Title,
							Action:     signature.Action,
							CureAction: SCureAction{0, 0},
						})

						// Если действие - удаление, то нет смысла проверять дальше
						if signature.Action == "delete" {
							break SignatureLoop
						}
					}
				}
			}
		}

		scannerSpeed = float64(time.Now().UnixNano()-startTimer) / float64(totalScannedBytes) / 1024
		// fmt.Printf("\r")
	}
}
